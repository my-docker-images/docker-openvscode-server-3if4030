
FROM gitlab-research.centralesupelec.fr:4567/my-docker-images/docker-openvscode-server-cpp

## software needed

ARG DEBIAN_FRONTEND="noninteractive"

RUN                             \
       apt-get update --yes     \
    && apt-get install --yes    \
         libgrpc++-dev          \
         libprotoc-dev          \
         libtirpc-dev           \
         # Maven package is quite old (3.6.3)
         # maven                \
         netbase                \
         pkg-config             \
         protobuf-compiler-grpc \
         python3-grpcio         \
         python3-venv           \
         openjdk-21-jdk         \
         rpcbind                \
         unzip                  \
    && apt-get autoremove       \
    && apt-get autoclean        \
    && rm -rf /var/lib/apt/lists/*

RUN \
       curl -o /tmp/apache-maven-3.9.9-bin.tar.gz https://dlcdn.apache.org/maven/maven-3/3.9.9/binaries/apache-maven-3.9.9-bin.tar.gz \
    && tar zxf /tmp/apache-maven-3.9.9-bin.tar.gz --directory=/opt                      \
    && rm /tmp/apache-maven-3.9.9-bin.tar.gz                                            \
    && mv /opt/apache-maven-3.9.9 /opt/maven                                            \
    && echo "export JAVA_HOME=/usr/lib/jvm/java-21-openjdk-amd64" >>/init-config/.zshrc \
    && echo "export PATH=/opt/maven/bin:\$PATH" >>/init-config/.zshrc

# Python module for grpc
RUN python3 -m pip install --break-system-packages grpcio-tools

## Axis2
RUN                                                              \
       curl -o /tmp/axis2-1.8.2-bin.zip https://dlcdn.apache.org/axis/axis2/java/core/1.8.2/axis2-1.8.2-bin.zip \
    && unzip -d /opt/ /tmp/axis2-1.8.2-bin.zip                   \
    && rm /tmp/axis2-1.8.2-bin.zip                               \
    && mv /opt/axis2-1.8.2 /opt/axis2                            \
    && chown -R abc:abc /opt/axis2                               \
    && echo "export AXIS2_HOME=/opt/axis2" >>/init-config/.zshrc \
    ## Do not display debug messages
    && sed -i -e 's/debug/warn/' /opt/axis2/conf/log4j2.xml      \
    ## using /bin/sh gives errors on if [[
    && sed -i -e 's/^sh /bash /' /opt/axis2/bin/java2wsdl.sh     \
    && sed -i -e 's/^sh /bash /' /opt/axis2/bin/wsdl2java.sh     \
    ## no more endorsed dirs
#    && sed -i -e '64d'           /opt/axis2/bin/axis2server.sh  \
    ## SimpleAxis2Server moved to kernel, script not updated
    && sed -i -e 's/transport.SimpleAxis2Server/kernel.SimpleAxis2Server/' /opt/axis2/bin/axis2server.sh \
    && echo "export PATH=/opt/axis2/bin:\$PATH" >>/init-config/.zshrc

## ActiveMQ
RUN                                                                    \
       curl -o /tmp/apache-activemq-5.18.6-bin.tar.gz https://dlcdn.apache.org/activemq/5.18.6/apache-activemq-5.18.6-bin.tar.gz \
    && tar zxf /tmp/apache-activemq-5.18.6-bin.tar.gz --directory=/opt \
    && rm /tmp/apache-activemq-5.18.6-bin.tar.gz                       \
    && mv /opt/apache-activemq-5.18.6 /opt/activemq                    \
    && chown -R abc:abc /opt/activemq                                  \
    && echo "export PATH=/opt/activemq/bin:\$PATH" >>/init-config/.zshrc

# Java extension for VS Code
RUN                                                                    \
    /app/openvscode-server/bin/openvscode-server                       \
        --extensions-dir /init-config/.openvscode-server/extensions    \
        --install-extension vscjava.vscode-java-pack

COPY settings.json /init-config/.openvscode-server/data/Machine/
COPY skel /init-config/workspace

# Start rpcbind at launch
RUN sed -i 's/^exec/\/etc\/init.d\/rpcbind start \&\& exec/' /usr/local/lib/wrapper_script.sh

# Port needed to access the web interface of the H2 database
EXPOSE 8082
