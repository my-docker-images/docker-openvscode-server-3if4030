
REPO = gitlab-research.centralesupelec.fr:4567/my-docker-images/docker-openvscode-server-3if4030
TAG  = latest

all:
	docker buildx create --use --node new-builder
	# 2025-02-21: The VSCode extension for Java crashes on linux/arm64
	#docker buildx build --provenance=false --push --platform "linux/amd64","linux/arm64" --tag "${REPO}:${TAG}" .
	docker buildx build --provenance=false --push --platform "linux/amd64" --tag "${REPO}:${TAG}" .

