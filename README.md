# docker-openvscode-server-3if4030

## What

This image is based on [docker-openvscode-server-cpp](https://gitlab-research.centralesupelec.fr/my-docker-images/docker-openvscode-server-cpp),
it adds some needed tools for the exercises of the course 3IF4030.

Included tools are:
- rpcbind
- jdk 21
- maven
- grpc
- activemq
- axis2

The skeleton files and READMEs of exercises are also included.

## Details

- The exposed ports are 3000 (browser access) and 8082 (other needs)
- The user folder is `/config`
- the user and sudo password is `abc`
- if docker is installed on your computer, you can run this 
  image, assuming you are in a specific folder that will be shared with the container at 
  `/config`, with:
  
  `docker run -p 3000:3000 -p 8082:8082 -v "$(pwd):/config"
    gitlab-research.centralesupelec.fr:4567/my-docker-images/docker-openvscode-server-3if4030`

- The image is amd64, but it can be used on other architectures using qemu or rosetta.
